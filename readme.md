# Fitness eshop test

Simple UI automated test based on Selenium & SELENIDE Framework

*Test scenario*:<br />
As a (not logged in) eshop customer,<br />
I want to go to the eshop<br />
When I am on eshop homepage, I want to open the product category page,<br />
On the selected product category page, I want to add two most expensive items from this category to the cart<br />
When I open the shopping cart I can see added items in the cart.

Tomas Susovsky, 2020
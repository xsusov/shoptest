package cz.tsusovsky;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Configuration;
import cz.tsusovsky.DTO.CartItem;
import cz.tsusovsky.DTO.ProductInfo;
import cz.tsusovsky.Pages.CartPage;
import cz.tsusovsky.Pages.HomePage;
import cz.tsusovsky.Pages.ProductCategoryPage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;

import static com.codeborne.selenide.Condition.*;

/**
 * Fitness eshop test
 *
 * @author tsusovsky
 * @date 08/25/2020
 */
public class FitnessTest {
    public final String ESHOP_TITLE = "4FITNESS.CZ | protein, kreatin, kolagen";
    public final String CATEGORY = "Balíky produktů";
    public final String SORT = "cena";
    public final String ORDER = "desc";

    ProductInfo firstProduct;
    ProductInfo secondProduct;

    @Before
    public void setup() {
        Configuration.baseUrl = "https://4fitness.cz";
    }

    @Test
    public void addToCartTwoMostExpensiveProductsInCategory() throws Exception {
        HomePage homePage = (HomePage) new HomePage().navigateTo();
        homePage.getTitle().should(text(ESHOP_TITLE));
        ProductCategoryPage categoryPage = homePage.selectCategory(CATEGORY);
        categoryPage.getTitle().should(text(CATEGORY));
        categoryPage.getProducts().shouldBe(CollectionCondition.sizeGreaterThanOrEqual(1));
        Assert.assertTrue("Before adding items to cart, cart should be empty.", categoryPage.getCartInfo().isEmpty);
        categoryPage.sortProducts(SORT, ORDER);
        List<ProductInfo> displayedProducts = categoryPage.getProductsInfos();
        assertProductsSortedByPriceDesc(displayedProducts);

        firstProduct = displayedProducts.get(0);
        categoryPage.addItemToCart(0);
        Assert.assertEquals(firstProduct.price.getCzkDecimal(), categoryPage.getCartInfo().getPriceCzk());
        secondProduct = displayedProducts.get(1);
        categoryPage.addItemToCart(1);
        BigDecimal expectedAddedProductsPriceCzk = firstProduct.price.getCzkDecimal().add(secondProduct.price.getCzkDecimal());
        Assert.assertEquals(expectedAddedProductsPriceCzk, categoryPage.getCartInfo().getPriceCzk());

        CartPage cart = (CartPage) categoryPage.goToCart();
        Assert.assertEquals(expectedAddedProductsPriceCzk, cart.getTotalPrice().getCzkDecimal());
        List<CartItem> cartItems = cart.getItems();
        Assert.assertEquals(2, cartItems.size());
        CartItem firstItem = matchItem(firstProduct, cartItems);
        assertCartItemMatchesProduct(firstItem, firstProduct, 1);
        CartItem secondItem = matchItem(secondProduct, cartItems);
        assertCartItemMatchesProduct(secondItem, secondProduct, 1);
    }

    private void assertProductsSortedByPriceDesc(List<ProductInfo> products) {
        for (int i = 0; i < products.size() - 1; i++) {
            ProductInfo current = products.get(i);
            ProductInfo next = products.get(i + 1);
            Assert.assertTrue(current.price.getCzkDecimal().compareTo(next.price.getCzkDecimal()) >= 0);
        }
    }

    private CartItem matchItem(ProductInfo product, List<CartItem> items) throws NoSuchElementException {
        return items.stream()
                .filter(i -> i.id.equals(product.id))
                .findAny()
                .orElseThrow(() -> new NoSuchElementException("cart item matching product " + product.toString() + " not found in shopping cart items."));
    }

    private void assertCartItemMatchesProduct(CartItem item, ProductInfo product, int expectedQuantity) {
        Assert.assertEquals(product.name, item.name);
        Assert.assertEquals(expectedQuantity, item.quantity);
        Assert.assertEquals(product.price, item.price);
    }
}

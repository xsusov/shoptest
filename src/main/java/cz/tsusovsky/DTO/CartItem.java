package cz.tsusovsky.DTO;

/**
 * DTO for items inside shopping cart
 *
 * @author tsusovsky
 * @date 08/27/2020
 */
public class CartItem {
    public final String id;
    public final String name;
    public final int quantity;
    public final Price price;

    public CartItem(String id, String name, int quantity, Price price) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

}

package cz.tsusovsky.DTO;

import cz.tsusovsky.Helper.PriceFormatter;

import java.math.BigDecimal;

/**
 * Price DTO - keep value stored as string
 *
 * @author tsusovsky
 * @date 08/26/2020
 */
public class Price {
    public final String czk;
    public final String eur;

    public Price(String czk, String eur) {
        this.czk = czk;
        this.eur = eur;
    }

    public BigDecimal getCzkDecimal() {
        return new BigDecimal(PriceFormatter.strip(czk));
    }

    public BigDecimal getEurDecimal() {
        return new BigDecimal(PriceFormatter.strip(eur));
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof Price)) {
            return false;
        }

        return this.getCzkDecimal().equals(((Price) other).getCzkDecimal())
                && this.getEurDecimal().equals(((Price) other).getEurDecimal());
    }

}

package cz.tsusovsky.DTO;

import java.math.BigDecimal;

/**
 * DTO for Cart info available from eshop menu
 * <p>
 * Numeric price value is parsed from price tags in format like:
 * CZK: 2 421 Kč
 * EUR: 16,14 €
 *
 * @author tsusovsky
 * @date 08/25/2020
 */
public class CartInfo {
    public final boolean isEmpty;
    public final Price price;

    public CartInfo(boolean isEmpty, Price price) {
        this.isEmpty = isEmpty;
        this.price = price;
    }

    public BigDecimal getPriceCzk() {
        return isEmpty ? new BigDecimal(0) : price.getCzkDecimal();
    }

    public BigDecimal getPriceTagEur() {
        return isEmpty ? new BigDecimal(0) : price.getEurDecimal();
    }
}

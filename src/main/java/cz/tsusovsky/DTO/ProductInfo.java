package cz.tsusovsky.DTO;

/**
 * DTO for product items in eshop data
 * <p>
 * Numeric price value is parsed from price tags in format like:
 * CZK: 2 421 Kč
 * EUR: 16,14 €
 *
 * @author tsusovsky
 * @date 08/26/2020
 */
public class ProductInfo {
    public final String id;
    public final String name;
    public final Price price;
    public final boolean available;

    public ProductInfo(String id, String name, Price price, boolean available) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.available = available;
    }

    @Override
    public String toString() {
        return ProductInfo.class.getName() + "{"
                + "id: " + id + ","
                + "name: " + name + ","
                + "price: " + price.czk + " / " + price.eur + ","
                + "available: " + (available ? "true" : "false") + ","
                + "}";
    }
}

package cz.tsusovsky.Pages;

import com.codeborne.selenide.SelenideElement;
import cz.tsusovsky.DTO.CartItem;
import cz.tsusovsky.DTO.Price;
import cz.tsusovsky.Helper.PriceFormatter;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.*;

/**
 * Shopping Cart Page Object
 *
 * @author tsusovsky
 * @date 08/27/2020
 */
public class CartPage implements IPage {

    public final String url = "/nakupni-kosik/";

    @Override
    public IPage navigateTo() {
        open(url);
        return this;
    }

    public Price getTotalPrice() {
        String totalTag = $(".total-price").text();
        return PriceFormatter.parsePriceTag(totalTag);
    }

    public List<CartItem> getItems() {
        return $$("form#frm-shoppingCartForm-form li.middle-xs").stream().map(this::parseCartItem).collect(Collectors.toList());
    }

    private CartItem parseCartItem(SelenideElement item) {
        String removeHref = item.$(".remove a.confirm").attr("href");
        String id = removeHref.substring(removeHref.lastIndexOf('/') + 1);
        String name = item.$("h2").text();
        int quantity = Integer.parseInt(item.$("input#shopping-cart-qty").val());
        Price price = PriceFormatter.parsePriceTag(item.$(".sum").text());

        return new CartItem(id, name, quantity, price);
    }
}

package cz.tsusovsky.Pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import cz.tsusovsky.DTO.Price;
import cz.tsusovsky.DTO.ProductInfo;
import cz.tsusovsky.Helper.PriceFormatter;
import org.openqa.selenium.By;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.*;

/**
 * Page Object for eshop page with products within single product category
 *
 * @author tsusovsky
 * @date 08/25/2020
 */
public class ProductCategoryPage implements IPage {
    private final By ITEMS_LOCATOR = By.xpath("//div[@class='product-item']");
    private final By SORT_RADIO_LOCATOR = By.xpath("//div[@id='category-filter']//form//input[@type='radio']");
    private final By ORDER_LOCATOR = By.xpath("//div[@id='category-filter']//div[@class='sipky']");

    public final String category;
    public final String url;

    public ProductCategoryPage(String category, String slug) {
        this.category = category;
        this.url = "/" + slug + "/";
    }

    @Override
    public IPage navigateTo() {
        open(url);
        return this;
    }

    public ElementsCollection getProducts() {
        return $$(ITEMS_LOCATOR);
    }

    public List<ProductInfo> getProductsInfos() {
        return getProducts().stream().map(this::parseProductInfo).collect(Collectors.toList());
    }

    public void addItemToCart(int productIndex) {
        String cartTotal = $(".cart-total").text();
        getProducts().get(productIndex).$(".add-to-cart-mt").click();
        $(".cart-total").waitUntil(Condition.not(Condition.text(cartTotal)), 2000, 250);
    }

    public void sortProducts(String sort, String order) {
        $(By.xpath("//div[@id='category-filter']//form//input[@type='radio'][@id='" + sort + "']//following-sibling::label")).click();
        $(By.xpath("//div[@id='category-filter']//div[@class='sipky']//a[contains(@class,'" + order.toLowerCase() + "')]")).click();
        $(By.xpath("//div[@id='category-filter']//div[@class='sipky']//a[contains(@class,'" + order.toLowerCase() + "')]")).waitUntil(Condition.cssClass("active"), 100);
    }

    public IPage goToCart() {
        $(".go-to-cart .cart-icon").click();
        return new CartPage();
    }

    private ProductInfo parseProductInfo(SelenideElement productElement) {
        String addByAjaxHref = productElement.$(".pro-action a").attr("href");
        String id = parseProductIdFromActionHref(addByAjaxHref);
        String name = productElement.$("h2").text();
        Price price = PriceFormatter.parsePriceTag(productElement.$(".regular-price").text());
        boolean available = productElement.$(".availability").text().equalsIgnoreCase("skladem");
        return new ProductInfo(id, name, price, available);
    }

    private String parseProductIdFromActionHref(String actionHref) {
        // action href like: /shopping-cart/?do=addToCart&idProduct=1022375&quantity=1&flavours=
        Pattern actionPattern = Pattern.compile(".*idProduct=(\\d+)&.*");
        Matcher matcher = actionPattern.matcher(actionHref);
        if (matcher.matches()) {
            return matcher.group(1);
        } else {
            return null;
        }
    }

}

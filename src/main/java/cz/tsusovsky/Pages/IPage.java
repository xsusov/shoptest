package cz.tsusovsky.Pages;

import com.codeborne.selenide.SelenideElement;
import cz.tsusovsky.DTO.CartInfo;
import cz.tsusovsky.DTO.Price;
import cz.tsusovsky.Helper.PriceFormatter;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;

/**
 * Common PO interface
 *
 * @author tsusovsky
 * @date 08/25/2020
 */
public interface IPage {

    IPage navigateTo();

    default SelenideElement getTitle() {
        return $("h1");
    }

    default String getTitleText() {
        return getTitle().getText();
    }

    default CartInfo getCartInfo() {
        boolean isEmpty = $(".cart-total").has(text("Košík je prázdný"));
        String priceTag = $(".cart-total").text();
        Price price = PriceFormatter.parsePriceTag(priceTag);
        return new CartInfo(isEmpty, price);
    }

}

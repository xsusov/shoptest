package cz.tsusovsky.Pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

/**
 * Fitness eshop homepage Page Object
 *
 * @author tsusovsky
 * @date 08/25/2020
 */
public class HomePage implements IPage {
    private final By ESHOP_MENU_LOCATOR = By.xpath("//*[@class='mega-menu-title']");
    private final By CATEGORY_MENU_ITEMS_LOCATOR = By.xpath("//*[@class='mega-menu-category']//li");

    public final String url = "/";

    @Override
    public IPage navigateTo() {
        open(url);
        return this;
    }

    public ProductCategoryPage selectCategory(String category) {
        $(ESHOP_MENU_LOCATOR).click();
        SelenideElement menuItem = $$(CATEGORY_MENU_ITEMS_LOCATOR).findBy(Condition.text(category));
        String slug = menuItem.$("a").attr("href").replaceAll("/", "");
        menuItem.click();
        return new ProductCategoryPage(category, slug);
    }

}

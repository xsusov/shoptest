package cz.tsusovsky.Helper;

import cz.tsusovsky.DTO.Price;
import org.apache.commons.lang3.StringUtils;

import static com.codeborne.selenide.Selenide.$;

/**
 * Utility for working with prices formatting used by eshop
 * CZK: 2 421 Kč
 * EUR: 16,14 €
 *
 * @author tsusovsky
 * @date 08/26/2020
 */
public class PriceFormatter {

    private PriceFormatter() {
    }

    public static String strip(String priceTag) {
        if (priceTag == null) {
            return null;
        }
        return priceTag.replaceAll(",", ".")
                .replaceAll("Kč", "")
                .replaceAll("€", "")
                .replaceAll(" ", "");
    }

    public static Price parsePriceTag(String priceTag) {
        priceTag = priceTag.replaceAll("Cena s DPH", "").replaceAll("\n", "").replaceAll("/","");
        String[] tags = priceTag.split("Kč");
        if (tags.length != 2) {
            return null;
        }
        String tagCzk = tags[0] + "Kč";
        String tagEur = tags[1];
        return new Price(tagCzk, tagEur);
    }
}
